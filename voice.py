import os
from python_speech_features import mfcc
import scipy.io.wavfile as wav
from sklearn.metrics import mean_squared_error


def calculate_mfcc(file_path):
    (rate, sig) = wav.read(file_path)
    return mfcc(sig, rate, nfft=2048)

samples_dir = '\samples\\'
test_dir = '\\test\\'

sample_voice_files = os.listdir(os.getcwd() + samples_dir)
test_voice_files = os.listdir(os.getcwd() + test_dir)

original_voices = []

for sample in sample_voice_files:
    original_voices.append(os.getcwd() + samples_dir + sample)

test_voices = []

for test in test_voice_files:
    test_voices.append(os.getcwd() + test_dir + test)

for original in original_voices:

    comparison_result = []
    original_mfcc = calculate_mfcc(original)

    for test_file in test_voices:
        test_mfcc = calculate_mfcc(test_file)
        error = mean_squared_error(original_mfcc, test_mfcc)
        comparison_result.append((test_file.split("\\")[-1], float(format(error, '.3f'))))

    comparison_result.sort(key=lambda x: x[1])
    print(original.split("\\")[-1])
    print(comparison_result)
    print('')
